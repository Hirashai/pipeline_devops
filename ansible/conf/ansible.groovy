#!groovy

def pipelineScript = new File('/var/jenkins_config/jobs/ansible-pipeline.groovy').getText("UTF-8")

pipelineJob('CaC/ansible') {
    description("Ansible")
    definition {
        cps {
            script(pipelineScript)
            sandbox()
        }
    }
}