#!groovy

pipeline {
    agent any
    tools {
        maven 'maven'
    }
    
    stages {

        stage('Clone') {
            steps {
                git branch: "${params.BRANCH}",
                    url: 'https://github.com/Ozz007/sb3t.git'
            }
        }
        stage('Compile') {
            steps {
                sh 'mvn compile'
            }
        }
        stage('Unit testing') {
            when {
            expression { params.SKIP_TESTS == false }
            }
            steps {
                sh 'mvn test'
            }
        }
        stage('Package') {
            steps {
                sh 'mvn package'
            }
        }
        stage('Integration testing') {
            when {
            expression { params.SKIP_TESTS == false }
            }
            steps {
                sh 'mvn verify'
            }
        }

        stage('Rename artifact') {
            steps {
                script{
                    sh "mv sb3t-ws/target/sb3t-ws-1.0-SNAPSHOT.jar java_app-${params.VERSION}-${params.VERSION_TYPE}.jar"
                }
            }
        }
    }
}