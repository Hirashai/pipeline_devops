#!groovy

def pipelineScript = new File('/var/jenkins_config/jobs/java_app-pipeline.groovy').getText("UTF-8")

pipelineJob('CI/java_app'){
    description("Builds artifact/jar for our java application")
    parameters {
        stringParam {
            name('BRANCH')
            defaultValue('master')
            description("Branch chosen to push")
            trim(false)
        }

        booleanParam {
            name('SKIP_TEST')
            defaultValue(true)
            description("Skip test")
        }

        stringParam {
            name('VERSION')
            defaultValue('1.0')
            description("Project's version")
            trim(false)
        }

        choice {
            name('VERSION_TYPE')
            choices(['SNAPSHOT', 'RELEASE'])
            description('Version type')
        }
    }

    definition {
        cps {
            script(pipelineScript)
            sandbox()
        }
    }
}