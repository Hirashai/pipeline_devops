#!groovy

pipeline {
    agent any
    stages {
        stage('Copy terraform files') {
            steps {
                sh 'cp -r /terraform_tf/* .'
            }
        }
        
        stage('Terraform init') {
            steps {
                sh 'terraform init'
            }
        }

        stage('Terraform plan') {
            steps {
                sh 'terraform plan'
            }
        }

        stage('Terraform apply') {
            steps {
                sh 'terraform apply -auto-approve'
            }
        }
    }
}