#!groovy

def pipelineScript = new File('/var/jenkins_config/jobs/terraform-apply-pipeline.groovy').getText("UTF-8")

pipelineJob('IaC/terraform-apply') {
    description("Create an AWS instance with Terraform")
    definition {
        cps {
            script(pipelineScript)
            sandbox()
        }
    }
}