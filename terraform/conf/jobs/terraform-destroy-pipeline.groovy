#!groovy

pipeline {
    agent any
    }
    stages {
        stage('Destroy instance') {
            steps {
                sh 'cd /var/jenkins_home/workspace/IaC/terraform-apply && terraform destroy -auto-approve'
            }
        }
    }
}