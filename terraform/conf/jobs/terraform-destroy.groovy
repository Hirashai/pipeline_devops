#!groovy

def pipelineScript = new File('/var/jenkins_config/jobs/terraform-destroy-pipeline.groovy').getText("UTF-8")

pipelineJob('IaC/terraform-destroy') {
    description("Destroy an AWS instance with Terraform")
    definition {
        cps {
            script(pipelineScript)
            sandbox()
        }
    }
}