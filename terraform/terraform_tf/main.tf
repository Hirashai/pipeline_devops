terraform {
    required_providers{
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.35"
        }
    }
}


provider "aws" {
    profile = var.aws_profile
    region = var.aws_region
    shared_credentials_file = "./.aws/credentials-ynov"
}

data "template_file" "user_data" {  
    template = file("./scripts/add-ssh-tf.yaml")
    }

resource "aws_volume_attachment" "ebs_att" {
    device_name = "dev/sdh"
    volume_id = aws_ebs_volume.disk.id
    instance_id = aws_instance.web.id
}

resource "aws_instance" "web" {
    key_name = var.ssh_key_name
    count = var.instance_count
    ami = var.ec2_ami
    instance_type = var.ec2_instance
    vpc_security_group_ids = [aws_security_group.cm_allow_tls.id]
    associate_public_ip_address = "true"
    user_data = data.template_file.user_data.rendered

    tags = {
        Name = "${var.env}-MATRAY_Clément-${count.index}"
        Environment = var.env
        Groups = "app"
        Owner = "clement.matray@ynov.com"
    }
}

resource "aws_ebs_volume" "disk" {
    availability_zone = var.aws_region
    size = 1
}

resource "aws_key_pair" "deployer" {
    key_name = var.ssh_key_name
    public_key = file("./ssh-keys/id_rsa_aws.pub")

}

resource "aws_security_group" "cm_allow_tls" {
    name = "SSH to VPC"
    description = "Allow SSH connections to VPC"
    
    ingress  {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 

    }

    ingress  {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 

    }

    egress  {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]

    }
    
} 

output "public_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = aws_instance.web.*.public_ip
}
