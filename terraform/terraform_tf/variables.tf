#variables config
#

variable "aws_region" {
    type = string
    default = "us-east-2"
}

variable "aws_profile" {
    type = string
    default = "default"
    }

variable "ec2_ami" {
    type = string
    default = "ami-0d97ef13c06b05a19"
    }

variable "ec2_instance" {
    type = string
    default = "t2.micro"
    }

variable "instance_count" {
    type = number
    default = 1
    }

variable "env" {
    type = string
    default = "dev"
    }

variable "ssh_key_name" {
    type = string
    default = "Clement_ssh"
    }
